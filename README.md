Run services
```
docker-compose up -d
```

Script for mv image1.jpeg <-> image2.jpeg :
```
docker/app/data/mv_images.sh
```

For invalidate image cache send request:
```
curl http://localhost:8080/image1.jpg -H "Pragma: true"
```